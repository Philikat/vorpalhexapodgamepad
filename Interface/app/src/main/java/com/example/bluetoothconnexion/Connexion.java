//package com.example.bluetoothtest;
//
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothSocket;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.util.Log;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//
//import java.util.Set;
//import java.util.UUID;
//
//import static android.content.ContentValues.TAG;
//
//public class Connexion {
//
//    private final String NOMROBOT = "VORPAL2224";
//    private final String ADRROBOT = "98:D3:31:F5:8B:97";
//
//    BluetoothAdapter bluetoothAdapter;
//    Set<BluetoothDevice> pairedDevices;
//
//    private BluetoothSocket comSocket = null;
//    private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
//    private ConnectedThread connecThread;
//
//
//
//    public Connexion() {
//        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
//        pairedDevices = bluetoothAdapter.getBondedDevices();
//
//
//    }
//
//    public void connect() {
//        BluetoothDevice vorpal = null;
//        BluetoothSocket tmp = null;
//            for (BluetoothDevice device : pairedDevices) {
//                if (device.getAddress().equals(ADRROBOT)) {
//                    vorpal = device;
//                }
//            }
//            if (vorpal != null) {
//
//
//                bluetoothAdapter.cancelDiscovery();
//                System.out.println(vorpal.toString());
//                SetupThread setup = new SetupThread(2);
//                System.out.println(setup.toString());
//                setup.start();
//                System.out.println("Début connexion");
//                comSocket=setup.connect(vorpal,MY_UUID);
//                System.out.println("fin connexion");
//                setup.interrupt();
//                System.out.println("lel");
//            }
//
//        System.out.println("lel");
//        connecThread = new ConnectedThread(comSocket);
//        System.out.println("lol");
//        connecThread.start();
//        System.out.println("je hais ma vie");
//    }
//
//    public void prepare(){
//        byte[] message = "@W1f".getBytes();
//        System.out.println("issou");
//        connecThread.write(message);
//    }
//
//    public void cancel() {
//            try {
//                comSocket.close();
//            } catch (IOException ex) {
//                Log.e(TAG, "Probleme lors de la fermeture", ex);
//            }
//    }
//
//    public Set<BluetoothDevice> getpairedDevices() {
//        return this.pairedDevices;
//    }
//
//
//}

package com.example.bluetoothconnexion;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncStatusObserver;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.content.ContentValues.TAG;

public class Connexion extends Thread{
    //Définition de constantes
    private final String ADRROBOT = "98:D3:31:F5:8B:97"; //Adresse MAC du robot
    private final int SEND_DELAY = 100; //Delai avant l'envoi d'une nouvelle trame au robot
    private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"); //UUID Permettant la création de la socket

    private BluetoothDevice vorpal; //Sert à récupérer les informations du Vorpal Hexapod
    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> pairedDevices; //Permet de récupérer les appareils connus du téléphone
    private BluetoothSocket comSocket; //Stocke la Socket de communication
    private DataOutputStream out; //Stocke le canal d'envoi de données
    private ArrayList<String> commandes; //Crée une file pour l'envoi des données


    public Connexion() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        pairedDevices = bluetoothAdapter.getBondedDevices();
        bluetoothAdapter.cancelDiscovery();
        commandes = new ArrayList<String>();
        this.start();
    }

    public void run() {
        BluetoothSocket tmp = null;
        //Si le bluetooth n'est pas activé, attendre 100 Millisecondes pour que le programme principale puisse le lancer
        while(!bluetoothAdapter.isEnabled()){
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //Récupère les informations du vorpal sur le téléphone
        vorpal = bluetoothAdapter.getRemoteDevice(ADRROBOT);
        //Si le vorpal n'est pas connu par le téléphone, on tente de s'y connecter
        if(vorpal==null){
            BroadcastReceiver receiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        // Discovery has found a device. Get the BluetoothDevice
                        // object and its info from the Intent.
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        if(device.getAddress().equals(ADRROBOT)) {
                            vorpal=device;
                        }
                    }
                }
            };
        }
        //Une fois la vorpal connu
        if (vorpal != null) {
            //Tant que la connexion n'est pas établie, l'application tente de s'y connecter
            while (comSocket == null) {
                try {
                    tmp = vorpal.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                    tmp.connect();
                    comSocket = tmp;
                    //Une fois la socket établie, on crée le channel d'envoi de données
                    out = new DataOutputStream(comSocket.getOutputStream());
                    sleep(SEND_DELAY);

                } catch (Exception ex) {
                    Log.e(TAG, "Probleme lors de la connexion", ex);
                }
            }
            try{
                System.out.println("CONNEXION OK");
                byte[] buffer;
                //Tant que la connexion n'est pas arrêté, on vérifie si des commandes sont en attented
                while (out != null){
                    if (!commandes.isEmpty()){
                        //On envoie au robot la première commande de la file, puis on défile cette commande
                        // et enfin on attends SLEEP_DELAY ms
                        buffer = commandes.get(0).getBytes();
                        comSocket.getOutputStream().write(buffer,0,buffer.length);
                        out.flush();
                        commandes.remove(0);
                        sleep(SEND_DELAY);
                    }

                }
            } catch (Exception ex) {
                Log.e(TAG, "Probleme lors de la connexion", ex);
            }
            System.out.println("fin connexion");

        }
    }

    //Sert à mettre dans la file d'attente les commandes de l'utilisateur
    public void prepare(String messageS) throws InterruptedException {
        sleep(SEND_DELAY);
        //Si la file n'est pas vide, on regarde la première commande de la file
        //Si la trame à enfiler est la même que la première trame de la file, on l'ajoute juste
        //Sinon on vide la file et on met la nouvelle trame.
        if(!commandes.isEmpty()) {
            if (commandes.get(0).equals(messageS)) {
                commandes.add(messageS);
            }
            else{
                commandes.clear();
                commandes.add(messageS);
            }
        }
        else{
            commandes.add(messageS);
        }

    }

    //Permet de terminer la connexion
    public void cancel() {
        try {
            comSocket.close();
            commandes.clear();
        } catch (IOException ex) {
            Log.e(TAG, "Probleme lors de la fermeture", ex);
        }
    }
}

