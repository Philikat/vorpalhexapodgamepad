package com.example.bluetoothconnexion;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener{
    BluetoothAdapter bluetoothAdapter;
    Connexion connexion=null;
    ImageButton mode1btn,mode2btn,mode3btn,subMode1Btn,subMode2Btn,subMode3Btn,subMode4Btn,AvBtn,ReBtn,DrBtn,GaBtn,SpBtn,conn;

    Integer sousMode=1;
    String mode="W";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Force l'application à se mettre en mode paysage
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Setup de tous les boutons de modes, sous-modes et déplacements
        mode1btn=findViewById(R.id.mode1But);
        mode2btn=findViewById(R.id.mode2But);
        mode3btn=findViewById(R.id.mode3But);

        mode1btn.setOnTouchListener(this);
        mode2btn.setOnTouchListener(this);
        mode3btn.setOnTouchListener(this);

        subMode1Btn=findViewById(R.id.subMode1But);
        subMode2Btn=findViewById(R.id.subMode2But);
        subMode3Btn=findViewById(R.id.subMode3But);
        subMode4Btn=findViewById(R.id.subMode4But);

        subMode1Btn.setOnTouchListener(this);
        subMode2Btn.setOnTouchListener(this);
        subMode3Btn.setOnTouchListener(this);
        subMode4Btn.setOnTouchListener(this);

        AvBtn=findViewById(R.id.AvBtn);
        ReBtn=findViewById(R.id.ReBtn);
        DrBtn=findViewById(R.id.DrBtn);
        GaBtn=findViewById(R.id.GaBtn);
        SpBtn=findViewById(R.id.SpBtn);


        AvBtn.setOnTouchListener(this);
        ReBtn.setOnTouchListener(this);
        DrBtn.setOnTouchListener(this);
        GaBtn.setOnTouchListener(this);
        SpBtn.setOnTouchListener(this);

        //Cache les boutons pour qu'ils ne soient visibles qu'une fois connectés au robot.
        cacherBoutons();

        //Création du bouton de connexion
        conn = findViewById(R.id.ConnBut);
        conn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //Si l'appli n'était pas connecté, lancer le thread de connexion et afficher les boutons
                if(connexion==null){
                    connexion= new Connexion();
                    showToast("Connexion établie avec succès, vous pouvez utiliser le robot");
                    revelerBoutons();
                }
                //Si l'appli était connecté, stopper le thread de connexion et cacher les boutons
                else{
                    connexion.cancel();
                    connexion=null;
                    cacherBoutons();
                    showToast("Connexion finie");
                }
            }
        });
    }

    //Premet de créer la trame du mode simplifié du robot
    protected String preparerTrame(String move){
        return "@"+this.mode+this.sousMode+move;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            switch (v.getId()) {
                //bouton de modes
                case R.id.mode1But:
                    changeMode("W");
                    break;
                case R.id.mode2But:
                    changeMode("D");
                    break;
                case R.id.mode3But:
                    changeMode("F");
                    break;

                //boutons de déplacement
                case R.id.AvBtn:
                    connexion.prepare(preparerTrame("f"));
                    break;
                case R.id.DrBtn:
                    connexion.prepare(preparerTrame("r"));
                    break;
                case R.id.GaBtn:
                    connexion.prepare(preparerTrame("l"));
                    break;
                case R.id.ReBtn:
                    connexion.prepare(preparerTrame("b"));
                    break;
                case R.id.SpBtn:
                    connexion.prepare(preparerTrame("s"));
                    break;

                //boutons de sous-mode
                case R.id.subMode1But:
                    changeSousMode(1);
                    connexion.prepare(preparerTrame(""));
                    break;
                case R.id.subMode2But:
                    changeSousMode(2);
                    connexion.prepare(preparerTrame(""));
                    break;
                case R.id.subMode3But:
                    changeSousMode(3);
                    connexion.prepare(preparerTrame(""));
                    break;
                case R.id.subMode4But:
                    changeSousMode(4);
                    connexion.prepare(preparerTrame(""));
                    break;
                default:
                    break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    //Change la variable sous mode de la classe pour l'envoi des trames
    protected void changeSousMode(int sm){
        this.sousMode = sm;

    }

    //Change la variable mode de la classe et change les images pour rendre visuelle le changement de mode
    protected void changeMode(String mode){
        this.mode=mode;
        switch(mode){
            case "W":
                subMode1Btn.setImageResource(R.drawable.bouton_walk_1 );
                subMode2Btn.setImageResource(R.drawable.bouton_walk_2 );
                subMode3Btn.setImageResource(R.drawable.bouton_walk_3 );
                subMode4Btn.setImageResource(R.drawable.bouton_walk_4 );
                break;
            case "D":
                subMode1Btn.setImageResource(R.drawable.bouton_dance_1 );
                subMode2Btn.setImageResource(R.drawable.bouton_dance_2 );
                subMode3Btn.setImageResource(R.drawable.bouton_dance_3 );
                subMode4Btn.setImageResource(R.drawable.bouton_dance_4 );
                break;
            case "F":
                subMode1Btn.setImageResource(R.drawable.bouton_fight_1 );
                subMode2Btn.setImageResource(R.drawable.bouton_fight_2 );
                subMode3Btn.setImageResource(R.drawable.bouton_fight_3 );
                subMode4Btn.setImageResource(R.drawable.bouton_fight_4 );
                break;
            default:
                break;
        }
    }

    //Sert à cacher les boutons
    private void cacherBoutons(){
        mode1btn.setVisibility(View.GONE);
        mode2btn.setVisibility(View.GONE);
        mode3btn.setVisibility(View.GONE);

        subMode1Btn.setVisibility(View.GONE);
        subMode2Btn.setVisibility(View.GONE);
        subMode3Btn.setVisibility(View.GONE);
        subMode4Btn.setVisibility(View.GONE);

        AvBtn.setVisibility(View.GONE);
        ReBtn.setVisibility(View.GONE);
        DrBtn.setVisibility(View.GONE);
        GaBtn.setVisibility(View.GONE);
        SpBtn.setVisibility(View.GONE);
    }

    //Sert à afficher les boutons
    private void revelerBoutons(){
        mode1btn.setVisibility(View.VISIBLE);
        mode2btn.setVisibility(View.VISIBLE);
        mode3btn.setVisibility(View.VISIBLE);

        subMode1Btn.setVisibility(View.VISIBLE);
        subMode2Btn.setVisibility(View.VISIBLE);
        subMode3Btn.setVisibility(View.VISIBLE);
        subMode4Btn.setVisibility(View.VISIBLE);

        AvBtn.setVisibility(View.VISIBLE);
        ReBtn.setVisibility(View.VISIBLE);
        DrBtn.setVisibility(View.VISIBLE);
        GaBtn.setVisibility(View.VISIBLE);
        SpBtn.setVisibility(View.VISIBLE);
    }

    //Simplifie l'affichage de message toast
    private void showToast(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

}
